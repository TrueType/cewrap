<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "cewrap".
 *
 * Auto generated 14-12-2018 18:37
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Content element wrap',
  'description' => 'Allows to wrap content elements with individual IDs and/or classes and select multiple predefined classes, e.g. to hide content elements dependent to screen width.',
  'category' => 'fe',
  'version' => '4.3.1',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' =>
  array (
    'depends' =>
    array (
      'typo3' => '8.7.0-9.5.99',
    ),
    'conflicts' =>
    array (
    ),
    'suggests' =>
    array (
      'cefooter' => '',
      'bootstrap_package' => '8.0.0-',
      'gridelements' => '',
    ),
  ),
);

